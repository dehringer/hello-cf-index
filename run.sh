#!/bin/bash

# This is a script to launch the app with some mocked out Cloud configuration that makes the app a bit more interesting when run locally.

VCAP_APPLICATION='{"instance_id":"451f045fd16427bb99c895a2649b7b2a","instance_index":3,"host":"0.0.0.0","port":61857,"started_at":"2013-08-1200:05:29 +0000","started_at_timestamp":1376265929,"start":"2013-08-12 00:05:29+0000","state_timestamp":1376265929,"limits":{"mem":256,"disk":1024,"fds":16384},"application_version":"c1063c1c-40b9-434e-a797-db240b587d32","application_name":"hello-cf-index","application_uris":["hello-cf-index.lmig.com"],"version":"c1063c1c-40b9-434e-a797-db240b587d32","name":"hello-cf-index","uris":["hello-cf-index.lmig.com"],"users":null}' \
VCAP_SERVICES='{"elephantsql": [{ "name": "elephantsql-c6c60","label": "elephantsql","tags": ["postgres","postgresql","relational"], "plan": "turtle", "credentials": {"uri": "postgres://seilbmbd:PHxTPJSbkcDakfK4cYwXHiIX9Q8p5Bxn@babar.elephantsql.com:5432/seilbmbd"}}],"sendgrid": [{"name": "mysendgrid","label": "sendgrid","tags": ["smtp"],"plan": "free","credentials": {"hostname": "smtp.sendgrid.net","username": "QvsXMbJ3rK","password": "HCHMOYluTv"}}]}' \
mvn clean spring-boot:run
