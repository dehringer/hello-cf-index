/*
 * Copyright (C) 2015, Liberty Mutual Group
 *
 * Created on Jun 22, 2015
 */
package com.lmig.cloudfoundry.examples.jobs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author David Ehringer (n0119737)
 */
@Component
public class ExampleService {

    private static final Log LOG = LogFactory.getLog(ExampleService.class);

    private final JobUtilities jobUtilities;

    @Autowired
    public ExampleService(JobUtilities jobUtilities) {
        this.jobUtilities = jobUtilities;
    }

    /**
     * This method will only run at the scheduled times on "instance 0" of your app.
     */
    @Scheduled(cron = JobSchedules.EXAMPLE_JOB)
    public void runExampleJob() {
        if (jobUtilities.isNotInstanceZero()) {
            LOG.info(" Not running runExampleJob because this isn't instance 0.");
            return;
        }
        LOG.info("Running runExampleJob.");
        // Profit..
    }
}
