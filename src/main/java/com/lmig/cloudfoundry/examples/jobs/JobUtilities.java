/*
 * Copyright (C) 2015, Liberty Mutual Group
 *
 * Created on Jun 22, 2015
 */
package com.lmig.cloudfoundry.examples.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.Cloud;
import org.springframework.cloud.app.ApplicationInstanceInfo;
import org.springframework.stereotype.Component;

/**
 * @author David Ehringer (n0119737)
 */
@Component
public class JobUtilities {

    private static final String INSTANCE_INDEX = "instance_index";

    private final Cloud cloud;

    @Autowired
    public JobUtilities(Cloud cloud) {
        this.cloud = cloud;
    }

    public boolean isNotInstanceZero() {
        return !isInstance(0);
    }

    /**
     * We always assume multiple instances of an application are running. Some
     * schedule tasks should only run once if they are going to run. At the same
     * time, those tasks may not absolutely have to run everytime. If a cycle is
     * missed, it is ok. In those cases, we can choose to only run the job on
     * instance "0." But we need to be aware that it is possible that instance 0
     * may not be running when the schedule task is scheduled to run.
     * <p/>
     * <strong>If this is run in a non-Cloud Foundry runtime, this method will
     * always return true</strong>
     * 
     * @return
     */
    private boolean isInstance(int number) {
        ApplicationInstanceInfo instanceInfo = cloud.getApplicationInstanceInfo();
        if (instanceInfo.getProperties().containsKey(INSTANCE_INDEX)) {
            Integer index = (Integer) cloud.getApplicationInstanceInfo().getProperties()
                    .get(INSTANCE_INDEX);
            return index == number;
        }
        return true;
    }
}
