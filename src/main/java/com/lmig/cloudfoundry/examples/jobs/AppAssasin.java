/*
 * Copyright (C) 2015, Liberty Mutual Group
 *
 * Created on Jul 24, 2015
 */
package com.lmig.cloudfoundry.examples.jobs;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author David Ehringer (n0119737)
 */
@Component
public class AppAssasin {

    private static final Log LOG = LogFactory.getLog(AppAssasin.class);

    private final AtomicBoolean kill = new AtomicBoolean();

    public void killApp(){
        kill.set(true);
    }
    
    @Scheduled(fixedDelay = 2000)
    void execute() {
        if (kill.getAndSet(false)) {
            LOG.fatal("Killing app...");
            System.exit(1);
        }
    }
}
