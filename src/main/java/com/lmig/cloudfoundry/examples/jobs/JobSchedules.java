/*
 * Copyright (C) 2015, Liberty Mutual Group
 *
 * Created on Jun 22, 2015
 */
package com.lmig.cloudfoundry.examples.jobs;

/**
 * @author David Ehringer (n0119737)
 */
public abstract class JobSchedules {

    public static final String EXAMPLE_JOB = "0/10 * * * * *";
    
}
