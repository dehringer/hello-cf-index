/*
 * Copyright (C) 2015, Liberty Mutual Group
 *
 * Created on Apr 20, 2015
 */
package com.lmig.cloudfoundry.examples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.Cloud;
import org.springframework.cloud.CloudFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author David Ehringer (n0119737)
 */
@SpringBootApplication
@EnableScheduling
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    /**
     * The Cloud instance is registered as a Spring bean because the CloudFactory is relatively
     * expensive to instantiate. 
     */
    @Bean
    public Cloud cloud(){
        CloudFactory cloudFactory = new CloudFactory();
        return cloudFactory.getCloud();
    }
}
