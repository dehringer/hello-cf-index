/*
 * Copyright (C) 2015, Liberty Mutual Group
 *
 * Created on Apr 20, 2015
 */
package com.lmig.cloudfoundry.examples;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.Cloud;
import org.springframework.cloud.CloudFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lmig.cloudfoundry.examples.jobs.AppAssasin;

/**
 * @author David Ehringer (n0119737)
 */
@Controller
public class IndexController {
    
    private final Cloud cloud;
    private final AppAssasin assasin;

    @Autowired
    public IndexController(AppAssasin killer) {
        CloudFactory cloudFactory = new CloudFactory();
        this.cloud = cloudFactory.getCloud();
        this.assasin = killer;
    }

    @RequestMapping("/")
    public String home(Model model) {
        populateModel(model);
        return "home";
    }

    @RequestMapping("/details")
    public String details(Model model) {
        populateModel(model);
        return "details";
    }

    private void populateModel(Model model) {
        Map<String, Object> props = cloud.getApplicationInstanceInfo().getProperties();
        model.addAttribute("index", cloud.getApplicationInstanceInfo().getAppId());
        model.addAttribute("instance_index", props.get("instance_index"));
        model.addAttribute("instance_id", props.get("instance_id"));
        model.addAttribute("port", props.get("port"));
        model.addAttribute("app_instance_properties", cloud.getApplicationInstanceInfo()
                .getProperties());
        model.addAttribute("services", beautifyVcapServices());
    }

    @SuppressWarnings("unchecked")
    private Object beautifyVcapServices() {
        Map<String, Object> services = new HashMap<String, Object>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String vcapServices = System.getenv("VCAP_SERVICES");
            if(vcapServices == null){
                vcapServices = "{}";
            }
            services.putAll(objectMapper.readValue(vcapServices, Map.class));
        } catch (JsonParseException e) {
            // just an example project
            e.printStackTrace();
        } catch (JsonMappingException e) {
            // just an example project
            e.printStackTrace();
        } catch (IOException e) {
            // just an example project
            e.printStackTrace();
        }
        return services;
    }
    
    @RequestMapping(value = "/kill", method = RequestMethod.POST)
    public String details() {
        assasin.killApp();
        return "redirect:/";
    }
    
}
