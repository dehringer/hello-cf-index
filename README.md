# Hello CF Index

A simple Java Web App that displays information about the specific instance of the app
that serves a request.  It also shows how to limit scheduled tasks to running on no more 
than one instance at a time (see below).

## Building the App

```
mvn clean package
```

## Deploying to Cloud Foundry

```
cf push
```
The configuration in `manifest.yml` will be used.

To view the effects of scaling your app, increase the instance count. After the additional
instances start, repeatedly refresh your browser to see the instance count change.
```
cf scale hello-cf-index -i 4
```

To see how services appear in `VCAP_SERVICES`, either provision and bind some services or 
create a user provided service and bind it to the app. You must restage your app for
the services to be available to your app. For example:

```
cf cups example-service -p '{"username":"my-username","password":"secret", "url":"jdbc:mysql://my-server/example-db"}'
cf bind-service hello-cf-index example-service
cf restage hello-cf-index
```

## Running locally

This app shows features of CF that require having some additional configuration stubbed out to run locally to actually see anything interesting.  **Most apps don't need this for running locally.**

### Just running it

You can run the app with below.  You just won't see any `VCAP_APPLICATION` and `VCAP_SERVICES` data displayed.

```
mvn clean spring-boot:run -Dspring.cloud.appId=hello-cf-index 
```

### Using a script to mock the cloud

This script mocks out some environment variables so that you will see some data when running locally.

```
./run.sh
```

### Using spring-cloud-local-config directly

To run locally with spring-cloud-local-config, use the following. It provides a stubbed out `VCAP_APPLICATION` and `VCAP_SERVICES` environment variable.

```
VCAP_APPLICATION='{"instance_id":"451f045fd16427bb99c895a2649b7b2a","instance_index":3,"host":"0.0.0.0","port":61857,"started_at":"2013-08-1200:05:29 +0000","started_at_timestamp":1376265929,"start":"2013-08-12 00:05:29+0000","state_timestamp":1376265929,"limits":{"mem":256,"disk":1024,"fds":16384},"application_version":"c1063c1c-40b9-434e-a797-db240b587d32","application_name":"hello-cf-index","application_uris":["hello-cf-index.lmig.com"],"version":"c1063c1c-40b9-434e-a797-db240b587d32","name":"hello-cf-index","uris":["hello-cf-index.lmig.com"],"users":null}' \
VCAP_SERVICES='{"elephantsql": [{ "name": "elephantsql-c6c60","label": "elephantsql","tags": ["postgres","postgresql","relational"], "plan": "turtle", "credentials": {"uri": "postgres://seilbmbd:PHxTPJSbkcDakfK4cYwXHiIX9Q8p5Bxn@babar.elephantsql.com:5432/seilbmbd"}}],"sendgrid": [{"name": "mysendgrid","label": "sendgrid","tags": ["smtp"],"plan": "free","credentials": {"hostname": "smtp.sendgrid.net","username": "QvsXMbJ3rK","password": "HCHMOYluTv"}}]}' \
java -Dspring.cloud.appId=hello-cf-index -jar target/hello-cf-index-1.0.0.jar
```

## Scheduled Jobs and Running on a single instance

When you are running multiple instances of your app in CF, there are a variety of ways to 
have an internally scheduled job run on just a single instance.  This example app shows
one of those approaches. See `ExampleService` and associated classes for details.  This approach uses
Spring Cloud (which doesn't actually depend on the Spring Framework and doesn't require that you use Spring).  

To see the code in action, deploy your app as shown above. Scale your app to more than one instance:
```
cf scale hello-cf-index -i 4
```
You will see only 1 instance is running the "job."